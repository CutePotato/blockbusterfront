import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Juego } from '../juego';
import { Compania } from 'src/app/companias/compania';

@Component({
  selector: 'app-editar-juego',
  templateUrl: './editar-juego.component.html'
})
export class EditarJuegoComponent{

  categorias = [
    "ACCION",
    "AVENTURAS",
    "ESTRATEGIA",
    "RPG",
    "SHOOTER"
  ];

  fecha = new FormControl();

  @Input() juego:Juego;
  @Input() companias:Compania[];
  @Output() updatedJuego = new EventEmitter<Juego>();

  juegoUpdated(){
    this.updatedJuego.emit(this.juego);
  }

  formatFechaLanzamiento(fecha:Date):void{
    this.juego.fechaLanzamiento = fecha.toString();
  }

  compareCompany(companyToCompare: Compania, companySelected: Compania) {
    if (!companyToCompare || !companySelected) {
      return false;
    }

    return companyToCompare.id === companySelected.id;
  }
}

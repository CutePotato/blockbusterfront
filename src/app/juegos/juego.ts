import { Compania } from '../companias/compania';

export class Juego {
    id:number;
    titulo: string;
    fechaLanzamiento: string;
    categoria: string;
    pegi: number;
    companias:Compania[];
}

import { Component, EventEmitter, Output, Input } from '@angular/core';
import { Juego } from '../juego';
import { Compania } from 'src/app/companias/compania';

@Component({
  selector: 'app-crear-juego',
  templateUrl: './crear-juego.component.html'
})
export class CrearJuegoComponent{
  @Input() companias;
  @Output() newJuego = new EventEmitter<Juego>();

  categorias = [
    "ACCION",
    "AVENTURAS",
    "ESTRATEGIA",
    "RPG",
    "SHOOTER"
  ];

  juego:Juego={
    "id": null,
    "categoria": this.categorias[0],
    "fechaLanzamiento": null,
    "titulo":"PEPE",
    "pegi": 18,
    "companias": null
  }

  crearJuego(){
    this.newJuego.emit(this.juego);
  }

  formatFechaLanzamiento(fecha:Date):void{
    this.juego.fechaLanzamiento = fecha.toString();
  }

  compareCompany(companyToCompare: Compania, companySelected: Compania) {
    if (!companyToCompare || !companySelected) {
      return false;
    }

    return companyToCompare.id === companySelected.id;
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Juego } from './juego';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError, of } from 'rxjs';
import {JuegoRoutes} from '../rutas/juego.routes';
import { Stock } from '../stocks/stock';
import { Compania } from '../companias/compania';
import { AlertService } from '../_alert/alert.service';
import { LoginService } from '../login/login.service';

@Injectable()
export class JuegoService {

  private urlBase = "http://localhost:6666/juego/";

  constructor(private http: HttpClient, private alertService: AlertService, private loginService:LoginService) { }
  
  /*formatFechaLanzamiento(juego:Juego):Juego{
    this.juego.fechaLanzamiento = fecha.toString();
  }*/

  readJuegos(): Observable<Juego[]>{
    return this.http.get(JuegoRoutes.urlReadAll, {
      headers: this.loginService.getAuthHeaders()
    }).pipe(
      map(response=>response as Juego[]),
      catchError(e=>{
        this.alertService.error("Peto al intentar traer los Juegos");
        return throwError(e);
      })
    );
  }
  
  createJuego(body:Juego):Observable<Juego>{
    return this.http.post<Juego>(JuegoRoutes.urlCreate, body, {
      headers:{
        "Content-Type": "application/json"
      }
    }).pipe(
      map(response=>response as Juego)
    );
  }
  
  updateJuego(body:Juego):Observable<Juego>{
    return this.http.put<Juego>(this.urlBase+body.id+"/update", body,{
      headers:{
        "Content-Type": "application/json"
      }
    }).pipe(
      map(response => response as Juego)
    );
  }

  deleteJuego(body:Juego):Observable<Juego>{
    return this.http.delete<Juego>(this.urlBase+body.id+"/delete").pipe(
      map(response => response as Juego)
    );
  }
  //No hecho
  addStockJuego(titulo:string, body:Stock){
    return this.http.post(JuegoRoutes.urlAddStockJuego, body, {
      params: {
        "titulo": titulo
      }
    })
  }
  
  readStockJuego(titulo:string):Observable<Stock[]>{
    return this.http.get<Stock[]>(JuegoRoutes.urlReadStockJuego).pipe(
      map(response=>response as Stock[])
    )
  }
  
  updateStockJuego(titulo:string, body:Stock){
    return this.http.post(JuegoRoutes.urlUpdateStockJuego, body, {
      params: {
        "titulo": titulo
      }
    })
  }
  
  deleteStockJuego(titulo:string, body:Stock){
    return this.http.post(JuegoRoutes.urlDeleteStockJuego, body, {
      params: {
        "titulo": titulo
      }
    })
  }
  
  addCompJuego(titulo:string, body:Compania){
    return this.http.post(JuegoRoutes.urlAddCompJuego, body, {
      params: {
        "titulo": titulo
      }
    })
  }
  
  readCompJuego(titulo:string):Observable<Compania[]>{
    return this.http.get<Compania[]>(JuegoRoutes.urlReadCompJuego, {
      params: {
        "titulo": titulo
      }
    }).pipe(
      map(response=>response as Compania[])
    );
  }
  
  updateCompJuego(titulo:string, body:Compania){
    return this.http.post(JuegoRoutes.urlUpdateCompJuego, body, {
      params: {
        "titulo": titulo
      }
    })
  }
  
  deleteCompJuego(titulo:string, body:Compania){
    return this.http.post(JuegoRoutes.urlDeleteCompJuego, body, {
      params: {
        "titulo": titulo
      }
    })
  }
}

import { OnInit, Component } from '@angular/core';
import { JuegoService } from './juego.service';
import { Juego } from './juego';
import { Router } from '@angular/router';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { isUndefined } from 'util';
import { CompaniaService } from '../companias/compania.service';
import { Compania } from '../companias/compania';
@Component({
    selector: 'app-juegos',
    templateUrl: './juegos.component.html'
})
export class JuegosComponent implements OnInit{
    juegos:Juego[];
    companias:Compania[];
    juegoStart:Juego = {
        "id":1,
        "categoria":"ACCION",
        "fechaLanzamiento":"2000-02-02",
        "titulo": "Dark",
        "pegi": 18,
        "companias":[]
    };
    selectedJuego:Juego = this.juegoStart;
    ngOnInit(){
        this.juegoService.readJuegos().subscribe(
            juegos => this.juegos = juegos
        )
        this.companiaService.readCompañias().subscribe(
            companias => this.companias = companias
        )
    }
    constructor(private juegoService:JuegoService, private router:Router, private companiaService:CompaniaService){}

    listaCompanias(juego:Juego){
        if(isUndefined(juego.companias)){
            return "";
        }
        return juego.companias.map(e=>e.nombre).toString();
    }

    juegoUpdated(updatedJuego:Juego){
        console.log(updatedJuego);
        this.juegoService.updateJuego(updatedJuego).subscribe(
            response=>{
                console.log(response);
                var index = this.juegos.findIndex(e=>e.id == response.id);
                this.juegos.splice(index,1,response);
            }
        );
    }

    juegoCreate(newJuego:Juego){
        console.log(newJuego);
        this.juegoService.createJuego(newJuego).subscribe(
            response=>this.juegos.push(response)
        );
    }

    juegoDelete(juego:Juego){
        console.log(juego);
        this.juegoService.deleteJuego(juego).subscribe(
            response=>{
                var index = this.juegos.findIndex(e=>e.id == response.id);
                this.juegos.splice(index,1);
            }
        );
    }

    modalToggle(juego:Juego){
        this.selectedJuego = juego;
        this.close();
    }

    close(){
        var modal = document.getElementById("myModal");
        modal.classList.toggle("d-none");
        modal.classList.toggle("d-block");
    }

    toggleModalCrear(){
        var modal = document.getElementById("myModalCrear");
        modal.classList.toggle("d-none");
        modal.classList.toggle("d-block");
    }
    
}
import { Component, OnInit } from '@angular/core';
import { Compania } from './compania';
import { CompaniaService } from './compania.service';

@Component({
  selector: 'app-companias',
  templateUrl: './companias.component.html'
})
export class CompaniasComponent implements OnInit {

  companias: Compania[];
  startCompania:Compania = {
    "id": null,
    "cif": null,
    "nombre": null
  };
  selectedComp:Compania = this.startCompania;
  constructor(private companiaService:CompaniaService) { }

  ngOnInit(): void {
    this.companiaService.readCompañias().subscribe(
      companias => this.companias = companias
    );
  }

  crearToggle(){
    var modalCreate = document.getElementById("crear");
    modalCreate.classList.toggle("d-none");
    modalCreate.classList.toggle("d-block");
  }

  editarToggle(comp:Compania){
    this.selectedComp=comp;
    var modalEdit = document.getElementById("editar");
    modalEdit.classList.toggle("d-none");
    modalEdit.classList.toggle("d-block");
  }

  eliminar(comp:Compania){
    this.companiaService.deleteCompañia(comp).subscribe(
      response=>{
        var index = this.companias.findIndex(e=>e.id==response.id);
        this.companias.splice(index, 1);
      }
    )
  }

  crear(comp:Compania){
    this.companiaService.createCompañia(comp).subscribe(
      response=>{
        this.companias.push(response)
      }
    )
  }

  editar(comp:Compania){
    this.companiaService.updateCompañia(comp).subscribe(
      response=>{
        var index = this.companias.findIndex(e=>e.id==response.id);
        this.companias.splice(index, 1, response);
      }
    )
  }
}
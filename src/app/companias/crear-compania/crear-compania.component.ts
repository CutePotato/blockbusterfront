import { Component, Output, EventEmitter } from '@angular/core';
import { Compania } from '../compania';

@Component({
  selector: 'app-crear-compania',
  templateUrl: './crear-compania.component.html'
})
export class CrearCompaniaComponent{
  @Output() compEventoCrear = new EventEmitter<Compania>();

  compania:Compania = {
    "id":null,
    "cif":null,
    "nombre":null
  }

  emitir(){
    this.compEventoCrear.emit(this.compania);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {Compania} from "./compania";
import {CompaniaRoutes} from "./../rutas/compania.route";
import { Juego } from '../juegos/juego';
import { LoginService } from '../login/login.service';
import { AlertService } from '../_alert/alert.service';

@Injectable()
export class CompaniaService {

  private urlBase = "http://localhost:6666/comp/"

  constructor(private http: HttpClient, private loginService: LoginService,
    private alertService:AlertService, private router:Router) {}

  readCompañias():Observable<Compania[]>{
    return this.http.get<Compania[]>(CompaniaRoutes.urlReadAll, {
      headers: this.loginService.getAuthHeaders()
    }).pipe(
      map(response=>response as Compania[]),
      catchError(error => {
        console.error(`getCompanies error: "${error.message}"`);
        if (error.status == 401) {
          this.router.navigate(['/login']);
        } else {
          this.alertService.error(`Error al consultar las compañías: "${error.message}"`);
        }
        return throwError(error);
      })
    )
  }

  createCompañia(body:Compania):Observable<Compania>{
    return this.http.post<Compania>(CompaniaRoutes.urlCreate, body).pipe(
      map(response=>response as Compania)
    )
  }

  updateCompañia(body:Compania){
    return this.http.post<Compania>(this.urlBase+body.id+"/update", body).pipe(
      map(response=>response as Compania)
    );
  }

  deleteCompañia(body:Compania):Observable<Compania>{
    return this.http.delete<Compania>(this.urlBase+body.id+"/delete").pipe(
      map(response=>response as Compania)
    );
  }

  //Add Juego a Compañia
  addJuegoCompañia(cif:string, body:Compania){
    return this.http.post(CompaniaRoutes.urlCompAddJuego, body, {
      params: {
        "cif": cif
      }
    });
  }

  //Leer Juego de Compañia
  readJuegoCompañia(cif:string):Observable<Juego>{
    return this.http.get<Juego>(CompaniaRoutes.urlCompReadJuego, {
      params: {
        "cif": cif
      }
    }).pipe(
      map(response=>response as Juego)
    );
  }

  //Update Juego de Compañia
  updateJuegoCompañia(cif:string, body:Compania){
    return this.http.post(CompaniaRoutes.urlCompUpdateJuego, body, {
      params:{
        "cif": cif
      }
    });
  }

  //Delete Juego de Compañia
  deleteJuegoCompañia(cif:string, body:Compania){
    return this.http.post(CompaniaRoutes.urlCompDeleteJuego, body, {
      params:{
        "cif": cif
      }
    });
  }
}

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Compania } from '../compania';

@Component({
  selector: 'app-editar-compania',
  templateUrl: './editar-compania.component.html'
})
export class EditarCompaniaComponent{
  @Input() comp:Compania;
  @Output() compEventoEditar = new EventEmitter<Compania>();

  emitir(){
    this.compEventoEditar.emit(this.comp);
  }
}

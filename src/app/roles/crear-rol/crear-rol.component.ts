import { Component, EventEmitter, Output } from '@angular/core';
import { Rol } from '../rol';

@Component({
  selector: 'app-crear-rol',
  templateUrl: './crear-rol.component.html'
})
export class CrearRolComponent{
  @Output() newRol = new EventEmitter<Rol>();

  categorias = [
    "USER",
    "ADMIN",
    "TEST",//solo se puede crear este o
    "DEV" //este
  ];

  rol:Rol={
    "id": null,
    "rol":this.categorias[0]
  }

  crearRol(){
    this.newRol.emit(this.rol);
  }

}

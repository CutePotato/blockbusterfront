import { Injectable } from '@angular/core';
import { Rol } from './rol';
import { HttpClient } from '@angular/common/http';
import { RolRoutes } from '../rutas/rol.route';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Cliente } from '../clientes/cliente';

@Injectable()
export class RolService {

  private urlBase = "http://localhost:6666/rol/";

  constructor(private http: HttpClient) { }

  createRol(body:Rol):Observable<Rol>{
    return this.http.post<Rol>(RolRoutes.urlCreate, body).pipe(
      map(response=>response as Rol)
    );
  }

  readRol(rol:string):Observable<Rol>{
    return this.http.get<Rol>(RolRoutes.urlRead, {
      params: {
        "rol": rol
      }
    }).pipe(
      map(response=>response as Rol)
    );
  }

  readAllRol():Observable<Rol[]>{
    return this.http.get<Rol[]>(RolRoutes.urlReadAll).pipe(
      map(response=>response as Rol[])
    );
  }

  deleteRol(body:Rol):Observable<Rol>{
    console.log(this.urlBase+body.id+"/delete");
    return this.http.delete<Rol>(this.urlBase+body.id+"/delete").pipe(
      map(response=>response as Rol)
    );
  }

  addClientRol(rol:string, body:Rol){
    return this.http.post(RolRoutes.urlAddClient, body, {
      params:{
        "rol": rol
      }
    });
  }

  readClientRol(rol:string):Observable<Cliente[]>{
    return this.http.get<Cliente[]>(RolRoutes.urlReadClient, {
      params:{
        "rol": rol
      }
    }).pipe(
      map(response=>response as Cliente[])
    );
  }

  deleteClientRol(rol:string, body:Cliente){
    return this.http.post(RolRoutes.urlDeleteClient, body, {
      params:{
        "rol": rol
      }
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { Rol } from './rol';
import { RolService } from './rol.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html'
})
export class RolesComponent implements OnInit {

  constructor(private rolService:RolService) { }

  roles:Rol[];

  ngOnInit(): void {
    this.rolService.readAllRol().subscribe(
      roles => this.roles = roles
    );
  }

  modalToggle(){
      var modal = document.getElementById("myModal");
      modal.classList.toggle("d-none");
      modal.classList.toggle("d-block");
  }

  rolCreate(rol:Rol){
    this.rolService.createRol(rol).subscribe(
      e=>this.roles.push(e)
    )
  }

  rolDelete(rol:Rol){
    this.rolService.deleteRol(rol).subscribe(
      response=>{
        var index = this.roles.findIndex(e=>e.id == response.id);
        this.roles.splice(index,1);
      }
    )
  }
}

import { Component, OnInit } from '@angular/core';
import {Cliente} from './cliente';
import { ClienteService } from './cliente.service';
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html'
})
export class ClientesComponent implements OnInit {
  
  startClient:Cliente = {
    "id": null,
    "correo": null,
    "documento": null,
    "fechaNacimiento": null,
    "nombre": null,
    "password": null,
    "username": null
  };
  selectedCliente:Cliente = this.startClient;
  
  clientes: Cliente[];
  constructor(private clienteService: ClienteService) {}

  ngOnInit(): void {
    this.clienteService.readAllClientes().subscribe(
      clientes => this.clientes = clientes
    );
  }

  crear(cliente:Cliente){
    this.clienteService.createCliente(cliente).subscribe(
      response=>this.clientes.push(response)
    )
  }

  editar(cliente:Cliente){
    this.clienteService.updateCliente(cliente).subscribe(
      response=>{
        var index = this.clientes.findIndex(e=>e.id==response.id);
        this.clientes.splice(index, 1, response);
      }
    )
  }

  eliminar(cliente:Cliente){
    this.clienteService.deleteCliente(cliente).subscribe(
      response=>{
        var index = this.clientes.findIndex(e=>e.id==response.id);
        this.clientes.splice(index, 1);
      }
    )
  }

  crearToggle(){
    var modalCreate = document.getElementById("crear");
    modalCreate.classList.toggle("d-none");
    modalCreate.classList.toggle("d-block");
  }

  editarToggle(cliente:Cliente){
    this.selectedCliente = cliente;
    console.log(cliente);
    var modalCreate = document.getElementById("editar");
    modalCreate.classList.toggle("d-none");
    modalCreate.classList.toggle("d-block");
  }
}

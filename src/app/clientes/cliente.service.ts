import { Injectable } from '@angular/core';
import {Cliente} from './cliente';
import {of, Observable} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {map} from "rxjs/operators"
import { ClienteRoutes } from '../rutas/cliente.route';
import { Stock } from '../stocks/stock';
import { Rol } from '../roles/rol';

@Injectable()
export class ClienteService {

  constructor(private http: HttpClient) { }

  private urlBase = "http://localhost:6666/client/"

  readAllClientes():Observable<Cliente[]>{
    return this.http.get<Cliente[]>(ClienteRoutes.urlReadAll).pipe(
      map(response=>response as Cliente[]
    ));
  }

  createCliente(body:Cliente):Observable<Cliente>{
    return this.http.post<Cliente>(ClienteRoutes.urlCreate, body).pipe(
      map(response=>response as Cliente)
    );
  }

  updateCliente(body:Cliente):Observable<Cliente>{
    return this.http.post<Cliente>(this.urlBase+body.id+"/update", body).pipe(
      map(response=>response as Cliente)
    );
  }

  deleteCliente(body:Cliente):Observable<Cliente>{
    return this.http.delete<Cliente>(this.urlBase+body.id+"/delete").pipe(
      map(response=>response as Cliente)
    );
  }

  addStockCliente(documento:string, reference:string){
    return this.http.post(ClienteRoutes.urlAddStock, {}, {
      params:{
        "documento": documento,
        "reference": reference
      }
    })
  }

  readStockCliente(documento:string):Observable<Stock[]>{
    return this.http.get<Stock[]>(ClienteRoutes.urlReadStock, {
      params:{
        "documento": documento
      }
    }).pipe(
      map(response => response as Stock[])
    );
  }

  updateStockCliente(documento:string, body:Stock){
    return this.http.post(ClienteRoutes.urlUpdateStock, body, {
      params:{
        "documento": documento
      }
    });
  }

  deleteStockCliente(documento:string, body:Stock){
    return this.http.post(ClienteRoutes.urlDeleteStock, body, {
      params:{
        "documento": documento
      }
    });
  }

  addRolCliente(documento:string, rol:string){
    return this.http.post(ClienteRoutes.urlAddRol, {}, {
      params:{
        "documento": documento,
        "rol": rol
      }
    });
  }

  readRolCliente(username:string):Observable<Rol[]>{
    return this.http.get<Rol[]>(ClienteRoutes.urlReadRol, {
      params:{
        "username": username
      }
    }).pipe(
      map(response=>response as Rol[])
    );
  }

  deleteRolCliente(documento:string, rol:string){
    return this.http.post(ClienteRoutes.urlDeleteRol, {}, {
      params:{
        "documento": documento,
        "rol": rol
      }
    });
  }

}

import { Component, Output, EventEmitter } from '@angular/core';
import { Cliente } from '../cliente';

@Component({
  selector: 'app-crear-cliente',
  templateUrl: './crear-cliente.component.html'
})
export class CrearClienteComponent{
  @Output() clienteEventoCrear = new EventEmitter<Cliente>();

  cliente:Cliente={
    "id": null,
    "correo": "pepito@hola.es",
    "documento": "21341",
    "fechaNacimiento": new Date(),
    "nombre": "Pepe",
    "username": "pipito",
    "password": "1234"
  }

  emitir(){
    this.clienteEventoCrear.emit(this.cliente);
  }
}

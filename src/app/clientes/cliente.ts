import { Data } from '@angular/router';

export class Cliente {
    id:number;
    nombre: string;
    fechaNacimiento: Data;
    correo: string;
    documento: string;
    username: string;
    password: string;
}

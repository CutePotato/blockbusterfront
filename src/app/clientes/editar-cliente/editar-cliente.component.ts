import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Cliente } from '../cliente';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html'
})
export class EditarClienteComponent{
  @Input() cliente:Cliente;
  @Output() clienteEventoEditar = new EventEmitter<Cliente>();

  emitir(){
    console.log(this.cliente);
    this.clienteEventoEditar.emit(this.cliente);
  }
}

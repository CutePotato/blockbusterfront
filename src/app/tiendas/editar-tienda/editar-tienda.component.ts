import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Tienda } from '../tienda';

@Component({
  selector: 'app-editar-tienda',
  templateUrl: './editar-tienda.component.html'
})
export class EditarTiendaComponent {
  @Input() tienda:Tienda;
  @Output() tiendaEventoEditar = new EventEmitter<Tienda>();
  emitir(){
    this.tiendaEventoEditar.emit(this.tienda);
  }
}

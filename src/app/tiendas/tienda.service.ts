import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tienda } from './tienda';
import { TiendaRoutes } from '../rutas/tienda.route';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TiendaService {

  private urlBase="http://localhost:6666/tienda/";

  constructor(private http:HttpClient) { }

  readAllTiendas():Observable<Tienda[]>{
    return this.http.get<Tienda[]>(TiendaRoutes.urlReadAll).pipe(
      map(response=>response as Tienda[])
    )
  }

  createTienda(body:Tienda):Observable<Tienda>{
    return this.http.post<Tienda>(TiendaRoutes.urlCreate, body).pipe(
      map(response=>response as Tienda)
    );
  }

  updateTienda(body:Tienda):Observable<Tienda>{
    return this.http.put<Tienda>(this.urlBase+body.id+"/update", body).pipe(
      map(response=>response as Tienda)
    );
  }

  deleteTienda(body:Tienda):Observable<Tienda>{
    return this.http.delete<Tienda>(this.urlBase+body.id+"/delete").pipe(
      map(response=>response as Tienda)
    );
  }
}

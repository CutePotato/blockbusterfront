import { Component, OnInit } from '@angular/core';
import { Tienda } from './tienda';
import { TiendaService } from './tienda.service';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html'
})
export class TiendasComponent implements OnInit {

  startTienda:Tienda = {
    "id":null,
    "nombre":null,
    "direccion":null
  };
  selectedTienda:Tienda = this.startTienda;
  tiendas:Tienda[];

  constructor(private tiendaService:TiendaService) { }

  ngOnInit(): void {
    this.tiendaService.readAllTiendas().subscribe(
      response => this.tiendas = response
    )
  }

  crearToggle(){
    var modalCreate = document.getElementById("crear");
    modalCreate.classList.toggle("d-none");
    modalCreate.classList.toggle("d-block");
  }

  editarToggle(tienda:Tienda){
    this.selectedTienda=tienda;
    var modalEdit = document.getElementById("editar");
    modalEdit.classList.toggle("d-none");
    modalEdit.classList.toggle("d-block");
  }

  eliminar(tienda:Tienda){
    this.tiendaService.deleteTienda(tienda).subscribe(
      response=>{
        var index = this.tiendas.findIndex(e=>e.id==response.id);
        this.tiendas.splice(index, 1);
      }
    )
  }

  crear(tienda:Tienda){
    console.log(tienda);
    this.tiendaService.createTienda(tienda).subscribe(
      response=>{
        this.tiendas.push(response)
      }
    )
  }

  editar(tienda:Tienda){
    this.tiendaService.updateTienda(tienda).subscribe(
      response=>{
        var index = this.tiendas.findIndex(e=>e.id==response.id);
        this.tiendas.splice(index, 1, response);
      }
    )
  }
}

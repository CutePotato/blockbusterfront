import { Component, Output, EventEmitter } from '@angular/core';
import { Tienda } from '../tienda';

@Component({
  selector: 'app-crear-tienda',
  templateUrl: './crear-tienda.component.html'
})
export class CrearTiendaComponent{
  @Output() tiendaEventoCrear = new EventEmitter<Tienda>();
  tienda:Tienda={
    "id":null,
    "nombre":null,
    "direccion":null
  };

  emitir(){
    this.tiendaEventoCrear.emit(this.tienda);
  }
}

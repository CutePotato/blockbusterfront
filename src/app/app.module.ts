import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteService } from './clientes/cliente.service';
import { RouterModule, Routes } from "@angular/router";
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from "@angular/common/http";
import { JuegosComponent } from './juegos/juegos.component'
import { JuegoService } from './juegos/juego.service';
import { CompaniasComponent } from './companias/companias.component';
import { CompaniaService } from './companias/compania.service';
import { StocksComponent } from './stocks/stocks.component';
import { RolesComponent } from './roles/roles.component';
import { TiendasComponent } from './tiendas/tiendas.component';
import { StockService } from './stocks/stock.service';
import { TiendaService } from './tiendas/tienda.service';
import { RolService } from './roles/rol.service';
import { MainRoutes } from "./rutas/main.routes";
import { AlertService } from './_alert/alert.service';
import { AlertComponent } from './_alert/alert.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EditarJuegoComponent } from './juegos/editar-juego/editar-juego.component';
import { CrearJuegoComponent } from './juegos/crear-juego/crear-juego.component';
import { CrearRolComponent } from './roles/crear-rol/crear-rol.component';
import { CrearClienteComponent } from './clientes/crear-cliente/crear-cliente.component';
import { EditarClienteComponent } from './clientes/editar-cliente/editar-cliente.component';
import { CrearCompaniaComponent } from './companias/crear-compania/crear-compania.component';
import { EditarCompaniaComponent } from './companias/editar-compania/editar-compania.component';
import { CrearStockComponent } from './stocks/crear-stock/crear-stock.component';
import { EditarStockComponent } from './stocks/editar-stock/editar-stock.component';
import { CrearTiendaComponent } from './tiendas/crear-tienda/crear-tienda.component';
import { EditarTiendaComponent } from './tiendas/editar-tienda/editar-tienda.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    DirectivaComponent,
    ClientesComponent,
    HeaderComponent,
    CompaniasComponent,
    StocksComponent,
    RolesComponent,
    TiendasComponent,
    AlertComponent,
    JuegosComponent,
    EditarJuegoComponent,
    CrearJuegoComponent,
    CrearRolComponent,
    CrearClienteComponent,
    EditarClienteComponent,
    CrearCompaniaComponent,
    EditarCompaniaComponent,
    CrearStockComponent,
    EditarStockComponent,
    CrearTiendaComponent,
    EditarTiendaComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot(MainRoutes)
  ],
  providers: [
    ClienteService, JuegoService, StockService,
    CompaniaService, TiendaService, RolService,
    AlertService, LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

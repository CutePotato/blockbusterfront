import { Component, Output, EventEmitter } from '@angular/core';
import { Stock } from '../stock';

@Component({
  selector: 'app-crear-stock',
  templateUrl: './crear-stock.component.html'
})
export class CrearStockComponent{
  @Output() stockEventoCrear = new EventEmitter<Stock>();
  stock:Stock={
    "id":null,
    "estado":null,
    "reference":null
  };
  estados = [
    "ALQUILADO",
    "VENDIDO",
    "DISPONIBLE"
  ];

  emitir(){
    this.stockEventoCrear.emit(this.stock);
  }
}

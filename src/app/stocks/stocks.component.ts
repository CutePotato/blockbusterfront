import { Component, OnInit } from '@angular/core';
import { StockService } from './stock.service';
import { Stock } from './stock';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html'
})
export class StocksComponent implements OnInit {

  startStock:Stock = {
    "id": null,
    "estado": null,
    "reference": null
  };
  selectedStock:Stock = this.startStock;

  constructor(private stockService:StockService) { }

  stocks: Stock[];

  ngOnInit(): void {
    this.stockService.readAllStocks().subscribe(
      stocks => this.stocks = stocks
    );
  }

  crearToggle(){
    var modalCreate = document.getElementById("crear");
    modalCreate.classList.toggle("d-none");
    modalCreate.classList.toggle("d-block");
  }

  editarToggle(stock:Stock){
    this.selectedStock=stock;
    var modalEdit = document.getElementById("editar");
    modalEdit.classList.toggle("d-none");
    modalEdit.classList.toggle("d-block");
  }

  eliminar(stock:Stock){
    this.stockService.deleteStock(stock).subscribe(
      response=>{
        var index = this.stocks.findIndex(e=>e.id==response.id);
        this.stocks.splice(index, 1);
      }
    )
  }

  crear(stock:Stock){
    console.log(stock);
    this.stockService.createStock(stock).subscribe(
      response=>{
        this.stocks.push(response)
      }
    )
  }

  editar(stock:Stock){
    this.stockService.updateStock(stock).subscribe(
      response=>{
        var index = this.stocks.findIndex(e=>e.id==response.id);
        this.stocks.splice(index, 1, response);
      }
    )
  }
}

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Stock } from '../stock';

@Component({
  selector: 'app-editar-stock',
  templateUrl: './editar-stock.component.html'
})
export class EditarStockComponent {
  @Input() stock:Stock;
  @Output() stockEventoEditar = new EventEmitter<Stock>();
  estados = [
    "ALQUILADO",
    "VENDIDO",
    "DISPONIBLE"
  ];
  emitir(){
    this.stockEventoEditar.emit(this.stock);
  }
}

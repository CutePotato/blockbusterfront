import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StockRoutes } from '../rutas/stock.route';
import { Observable } from 'rxjs';
import { Stock } from './stock';
import { map } from 'rxjs/operators';

@Injectable()
export class StockService {

  private urlBase = "http://localhost:6666/stock/";

  constructor(private http:HttpClient) { }

  readAllStocks():Observable<Stock[]>{
    return this.http.get<Stock[]>(StockRoutes.urlReadAll).pipe(
      map(response=>response as Stock[])
    )
  }

  createStock(body:Stock):Observable<Stock>{
    return this.http.post<Stock>(StockRoutes.urlCreate, body).pipe(
      map(response=>response as Stock)
    );
  }

  updateStock(body:Stock):Observable<Stock>{
    return this.http.put<Stock>(this.urlBase+body.id+"/update", body).pipe(
      map(response=>response as Stock)
    );
  }

  deleteStock(body:Stock):Observable<Stock>{
    return this.http.delete<Stock>(this.urlBase+body.id+"/delete").pipe(
      map(response=>response as Stock)
    );
  }

}

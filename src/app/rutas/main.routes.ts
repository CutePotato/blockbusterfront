import {Routes} from "@angular/router";
import { DirectivaComponent } from '../directiva/directiva.component';
import { ClientesComponent } from '../clientes/clientes.component';
import { JuegosComponent } from '../juegos/juegos.component';
import { StocksComponent } from '../stocks/stocks.component';
import { CompaniasComponent } from '../companias/companias.component';
import { TiendasComponent } from '../tiendas/tiendas.component';
import { RolesComponent } from '../roles/roles.component';
import { LoginComponent } from '../login/login.component';

export const MainRoutes: Routes = [
    {path: '', redirectTo: '/clientes', pathMatch: 'full'},
    {path: 'directivas', component: DirectivaComponent},
    {path: 'clientes', component: ClientesComponent},
    {path: 'juegos', component: JuegosComponent},
    {path: 'stock', component: StocksComponent},
    {path: 'companias', component: CompaniasComponent},
    {path: 'tiendas', component: TiendasComponent},
    {path: 'roles', component: RolesComponent},
    {path: 'login', component: LoginComponent}
];
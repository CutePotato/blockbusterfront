export class RolRoutes{
    public static urlCreate:string = "http://localhost:6666/rol/create";
    public static urlRead:string = "http://localhost:6666/rol/read";
    public static urlReadAll:string = "http://localhost:6666/rol/read-all";
    public static urlDelete:string = "http://localhost:6666/rol/delete";

    public static urlAddClient:string = "http://localhost:6666/rol/add-client";
    public static urlReadClient:string = "http://localhost:6666/rol/read-client";
    public static urlDeleteClient:string = "http://localhost:6666/rol/delete-client";
}
export class JuegoRoutes{
    public static urlReadAll:string = "http://localhost:6666/juego/read-all";
    public static urlRead:string = "http://localhost:6666/juego/read";
    public static urlCreate:string = "http://localhost:6666/juego/create";
    public static urlUpdate:string = "http://localhost:6666/juego/{id}/update";
    public static urlDelete:string = "http://localhost:6666/juego/delete";

    public static urlAddStockJuego:string = "http://localhost:6666/juego/add-stock";
    public static urlReadStockJuego:string = "http://localhost:6666/juego/read-stock";
    public static urlUpdateStockJuego:string = "http://localhost:6666/juego/update-stock";
    public static urlDeleteStockJuego:string = "http://localhost:6666/juego/delete-stock";
    
    public static urlAddCompJuego:string = "http://localhost:6666/juego/add-comp";
    public static urlReadCompJuego:string = "http://localhost:6666/juego/read-comp";
    public static urlUpdateCompJuego:string = "http://localhost:6666/juego/update-comp";
    public static urlDeleteCompJuego:string = "http://localhost:6666/juego/delete-comp";

}
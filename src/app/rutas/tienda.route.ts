export class TiendaRoutes{
    public static urlCreate:string = "http://localhost:6666/tienda/create";
    public static urlRead:string = "http://localhost:6666/tienda/read";
    public static urlReadAll:string = "http://localhost:6666/tienda/read-all";
    public static urlUpdate:string = "http://localhost:6666/tienda/update";
    public static urlDelete:string = "http://localhost:6666/tienda/delete";

    public static urlAddStock:string = "http://localhost:6666/tienda/add-stock";
    public static urlReadStock:string = "http://localhost:6666/tienda/read-stock";
    public static urlUpdateStock:string = "http://localhost:6666/tienda/update-stock";
    public static urlDeleteStock:string = "http://localhost:6666/tienda/delete-stock";
}
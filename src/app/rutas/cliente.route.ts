export class ClienteRoutes{
    public static urlCreate:string = "http://localhost:6666/client/create";
    public static urlRead:string = "http://localhost:6666/client/read";
    public static urlReadAll:string = "http://localhost:6666/client/read-all";
    public static urlUpdate:string = "http://localhost:6666/client/update";
    public static urlDelete:string = "http://localhost:6666/client/delete";

    public static urlAddStock:string = "http://localhost:6666/client/add-stock";
    public static urlReadStock:string = "http://localhost:6666/client/read-stock";
    public static urlUpdateStock:string = "http://localhost:6666/client/update-stock";
    public static urlDeleteStock:string = "http://localhost:6666/client/delete-stock";

    public static urlAddRol:string = "http://localhost:6666/client/add-rol";
    public static urlReadRol:string = "http://localhost:6666/client/read-rol";
    public static urlDeleteRol:string = "http://localhost:6666/client/delete-rol";
}
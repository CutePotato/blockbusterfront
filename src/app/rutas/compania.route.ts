export class CompaniaRoutes{
    public static urlCreate:string = "http://localhost:6666/comp/create";
    public static urlRead:string = "http://localhost:6666/comp/read";
    public static urlReadAll:string = "http://localhost:6666/comp/read-all";
    public static urlUpdate:string = "http://localhost:6666/comp/update";
    public static urlDelete:string = "http://localhost:6666/comp/delete";
    
    public static urlCompAddJuego:string = "http://localhost:6666/comp/add-juego";
    public static urlCompReadJuego:string = "http://localhost:6666/comp/read-juego";
    public static urlCompUpdateJuego:string = "http://localhost:6666/comp/update-juego";
    public static urlCompDeleteJuego:string = "http://localhost:6666/comp/delete-juego";

}
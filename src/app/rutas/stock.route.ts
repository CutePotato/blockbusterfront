export class StockRoutes{
    public static urlCreate:string = "http://localhost:6666/stock/create";
    public static urlRead:string = "http://localhost:6666/stock/read";
    public static urlReadAll:string = "http://localhost:6666/stock/read-all";
    public static urlUpdate:string = "http://localhost:6666/stock/update";
    public static urlDelete:string = "http://localhost:6666/stock/delete";
}